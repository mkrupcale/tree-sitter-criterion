# tree-sitter-criterion

## Introduction

`tree-sitter-criterion` is a Criterion.rs[1] benchmark for the tree-sitter[2] library.

## Building

To build (on Fedora):

```shell
# Clone and enter the repository
git clone https://gitlab.com/mkrupcale/tree-sitter-criterion.git
cd tree-sitter-criterion
# Initialize submodules
git submodule update --init --recursive --jobs $(nproc)
# Build WebAssembly
podman run -v "$PWD":/src:Z trzeci/emscripten-slim .gitlab/ci/scripts/build-tree-sitter-wasm.sh
# Install build dependencies
sudo .gitlab/ci/scripts/install-dependencies.sh -b benchmarks
# Build tree-sitter and generate fixtures
.gitlab/ci/scripts/build-tree-sitter.sh
.gitlab/ci/scripts/generate-tree-sitter-fixtures.sh
# Copy the Cargo.lock file to tie crate versions
cp tree-sitter/Cargo.lock tree-sitter-criterion/Cargo.lock
# Build benchmarks
cd tree-sitter-criterion
cargo build --benches --release
```

### Requirements

Building `tree-sitter-criterion` requires building (a subset of) the `tree-sitter` submodule, which itself requires[3]:
 - C compiler
 - Rust toolchain: `rustc` and `cargo`
 - Docker (or other container runtime) or Emscripten compiler

The `tree-sitter-criterion` benchmarks depends on the Rust packages:
 - `criterion`
 - `lazy_static`
 - `tree-sitter`
 - `tree-sitter-cli`

## Usage

Once `tree-sitter-criterion` has been built, the benchmarks can be run using `cargo`:

```shell
cargo bench --bench benchmarks -- --verbose
```

See the Criterion user guide[4] for detailed usage.

To test different optimizations of `tree-sitter`, you can work with the `tree-sitter` git submodule as usual, rebuild, and rerun the benchmarks to compare changes in performance.

In addition to the benchmark comparison analysis Criterion can perform, it's possible to do a simple comparison using `critcmp`[5] with saved Criterion baselines.

### Benchmarks

The benchmarks are based on the same set of benchmarks tested by upstream. In particular, the benchmarks are parameterized on the variables:
 - parser
 - input

The parser can be given arbitrary input which either succeeds or fails to parse according to the parser's grammar.

## License

The contents of this repository are licensed under the MIT license, except where otherwise noted.

## References

1. [Criterion.rs](https://bheisler.github.io/criterion.rs/book/index.html)
2. [tree-sitter](http://tree-sitter.github.io/tree-sitter/)
3. [tree-sitter Contributing - Developing - Prerequisites](http://tree-sitter.github.io/tree-sitter/contributing#prerequisites)
4. [Criterion.rs - User Guide](https://bheisler.github.io/criterion.rs/book/user_guide/user_guide.html)
5. [critcmp](https://github.com/BurntSushi/critcmp)
