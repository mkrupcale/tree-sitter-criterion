#!/bin/sh

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

DEFAULT_CC=gcc
DEFAULT_CXX=g++

function print_usage ()
{
    echo "Usage: "
    echo "${BASH_SOURCE} [--help] <options> targets..."
    echo ""
    echo "Options"
    echo "  -h,--help"
    echo "    Prints help"
    echo "  -b,--build"
    echo "    Installs build dependencies"
    echo ""
    echo "Targets"
    echo "  all"
    echo "    Install all target dependencies"
    echo "  tree-sitter"
    echo "    Install all tree-sitter dependencies"
    echo "  benchmarks"
    echo "    Install all benchmarks dependencies"
}

# get_build_pkgs cc
function get_build_pkgs ()
{
    local cc="$1"
    pkgs+=("${cc}" "cargo" "git")
}

# get_compiler_pkgs cc [cxx]
function get_c_compiler_pkgs ()
{
    local cc="$1"
    local cxx="$2"
    pkgs+=("${cc}")
    if [ -n "${cxx}" ]; then
       if [ "${cxx}" == g++ -o "${cxx}" == clang++ ]; then
	   pkgs+=("${cxx}")
       else
	   if [ "${cc}" == gcc ]; then
               pkgs+=("gcc-c++")
	   fi
	   # clang++ is provided by clang package
       fi
    fi
}

# get_tree_sitter_pkgs build
function get_tree_sitter_pkgs ()
{
    :
}

# get_benchmarks_pkgs build cc
function get_benchmarks_pkgs ()
{
    local build="$1"
    local cc="$2"
    if [ -n "${build}" ]; then
        pkgs+=("findutils")
    else
	pkgs+=("gnuplot")
	get_c_compiler_pkgs "${cc}" "true"
    fi
}

# install_pkgs pkgs
function install_pkgs ()
{
    local pkgs=("$@")
    if [ "${#pkgs[@]}" -ne 0 ]; then
        rpm -q "${pkgs[@]}" || dnf -y install "${pkgs[@]}"
    fi
}

while [ $# -gt 0 ]; do
    case "$1" in
        -h|--help)
            print_usage
            exit 0
            ;;
        -b|--build)
            INSTALL_BUILD_DEPS=true
            ;;
        "tree-sitter")
            INSTALL_TREE_SITTER_DEPS=true
            TARGET_SPECIFIED=true
            ;;
        "benchmarks")
            INSTALL_TREE_SITTER_DEPS=true
            INSTALL_BENCHMARKS_DEPS=true
            TARGET_SPECIFIED=true
            ;;
    esac
    shift
done

if [ -z "${TARGET_SPECIFIED}" ]; then
    INSTALL_TREE_SITTER_DEPS=true
    INSTALL_BENCHMARKS_DEPS=true
fi

pkgs=()

cc="${cc:-${DEFAULT_CC}}"
cxx="${cxx:-${DEFAULT_CXX}}"

if [ "${INSTALL_BUILD_DEPS}" == true ]; then
    get_build_pkgs "${cc}"
fi

if [ "${INSTALL_TREE_SITTER_DEPS}" == true ]; then
    get_tree_sitter_pkgs "${INSTALL_BUILD_DEPS}"
fi

if [ "${INSTALL_BENCHMARKS_DEPS}" == true ]; then
    get_benchmarks_pkgs "${INSTALL_BUILD_DEPS}" "${cc}"
fi

install_pkgs "${pkgs[@]}"
