#!/bin/sh

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

set -e

cd tree-sitter

script/fetch-fixtures
script/generate-fixtures
