#!/bin/sh

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

set -e

cd tree-sitter

web_dir=lib/binding_web
exports=$(cat ${web_dir}/exports.json)
emscripten_flags="-O3"

export EMCC_FORCE_STDLIBS=libc++

mkdir -p target/scratch

emcc=emcc
# Use emscripten to generate `tree-sitter.js` and `tree-sitter.wasm`
# in the `target/scratch` directory
$emcc                                \
  -s WASM=1                          \
  -s TOTAL_MEMORY=33554432           \
  -s ALLOW_MEMORY_GROWTH=1           \
  -s MAIN_MODULE=2                   \
  -s NO_FILESYSTEM=1                 \
  -s "EXPORTED_FUNCTIONS=${exports}" \
  $emscripten_flags                  \
  -std=c99                           \
  -D 'fprintf(...)='                 \
  -I lib/src                         \
  -I lib/include                     \
  -I lib/utf8proc                    \
  --js-library ${web_dir}/imports.js \
  --pre-js ${web_dir}/prefix.js      \
  --post-js ${web_dir}/binding.js    \
  lib/src/lib.c                      \
  ${web_dir}/binding.c               \
  -o target/scratch/tree-sitter.js

mv target/scratch/tree-sitter.js $web_dir/tree-sitter.js
mv target/scratch/tree-sitter.wasm $web_dir/tree-sitter.wasm
